/*-----------------------------------------------------------------------------------
    Template Name: Nycondos Luxury Apartments HTML Template
    Template URI: https://webtend.biz/onitir
    Author: WebTend
    Author URI: https://www.webtend.com
    Version: 1.0

	Note: This is Main js File For custom and jQuery plugins activation Code..
-----------------------------------------------------------------------------------

/*---------------------------
	JS INDEX
	===================
	01. Main Menu
	02. Banner Slider
	03. ROOM Slider(Big)
	04. Testimonial Slider
	05. Latest Post Slider
	06. Feature Apartment Slider
	07. CounterUp
	08. Instagram Feed Slider
	09. Food Menu SLider
	10. Gallery Sliders & Popup
	11. Apartment Slider Two
	12. Banner Image Slider
	13. offCanvas Active
	14. init extra plugin
	15. Active Gallery And Video Popup
	16. Search Form
	17. Prelader
	18. Back to top
	19. Sticky header

-----------------------------*/

var mirandaDoc;

(function ($) {
	'use strict';
	mirandaDoc = {
		init: function () {
			this.mianMenu();
			this.extraPlugin();
			this.searchForm();
		},

		//===== 01. Main Menu
		mianMenu() {
			// Variables
			var var_window = $(window),
				navContainer = $('.nav-container'),
				pushedWrap = $('.nav-pushed-item'),
				pushItem = $('.nav-push-item'),
				pushedHtml = pushItem.html(),
				pushBlank = '',
				navbarToggler = $('.navbar-toggler'),
				navMenu = $('.nav-menu'),
				navMenuLi = $('.nav-menu ul li'),
				closeIcon = $('.navbar-close');

			// navbar toggler
			navbarToggler.on('click', function () {
				navbarToggler.toggleClass('active');
				navMenu.toggleClass('menu-on');
			});

			// close icon
			closeIcon.on('click', function () {
				navMenu.removeClass('menu-on');
				navbarToggler.removeClass('active');
			});

			// adds toggle button to li items that have children
			navMenu.find('li a').each(function () {
				if ($(this).next().length > 0) {
					$(this)
						.parent('li')
						.append(
							'<span class="dd-trigger"><i class="fal fa-angle-down"></i></span>'
						);
				}
			});

			// expands the dropdown menu on each click
			navMenu.find('li .dd-trigger').on('click', function (e) {
				e.preventDefault();
				$(this)
					.parent('li')
					.children('ul')
					.stop(true, true)
					.slideToggle(350);
				$(this).parent('li').toggleClass('active');
			});

			// check browser width in real-time
			function breakpointCheck() {
				var windoWidth = window.innerWidth;
				if (windoWidth <= 991) {
					navContainer.addClass('breakpoint-on');

					pushedWrap.html(pushedHtml);
					pushItem.hide();
				} else {
					navContainer.removeClass('breakpoint-on');

					pushedWrap.html(pushBlank);
					pushItem.show();
				}
			}

			breakpointCheck();
			var_window.on('resize', function () {
				breakpointCheck();
			});
		},

		

		//===== 15. init extra plugin
		extraPlugin() {

			// init datepicker
			$('#arrival-date, #departure-date').datepicker({
				format: 'd-m-yyyy',
				autoclose: true,
			});

			// init wow js
			new WOW().init();

			// init isotope on features
			$('.fetaure-masonary').isotope();
		},

		//===== 17. Search Form
		searchForm() {
			// Set Click Function For open
			$('#searchBtn').on('click', function (e) {
				e.preventDefault();
				$('.search-form').slideToggle(350);
				$(this).toggleClass('active');
			});
		},
	};

	// Document Ready
	$(document).ready(function () {
		mirandaDoc.init();
	});

	// Window Load
	$(window).on('load', function () {
		//===== 17. Preloader
		$('.preloader').fadeOut('slow', function () {
			$(this).remove();
		});

		//===== 18. Back to top
		$('#backToTop').on('click', function (e) {
			e.preventDefault();
			$('html, body').animate(
				{
					scrollTop: '0',
				},
				1200
			);
		});
	});

	// Window Scroll
	$(window).on('scroll', function () {
		//===== 19. Sticky header
		var scroll = $(window).scrollTop();
		if (scroll < 150) {
			$('.sticky-header').removeClass('sticky-active');
		} else {
			$('.sticky-header').addClass('sticky-active');
		}

		//===== 20. Scroll Event on back to top
		if (scroll > 300) $('#backToTop').addClass('active');
		if (scroll < 300) $('#backToTop').removeClass('active');
	});
})(jQuery);