const COUNTRY_LIST = ["Afghanistan","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea",,"Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];

const selectMenu = document.getElementById('selectMenu');
const selectCityMenu = document.getElementById('selectCityMenu');
const selectBtn = document.getElementById('selectBtn');
const selectCityBtn = document.getElementById('selectCityBtn');
const selectBtnSpan = document.querySelector('#selectBtn span');
const selectCityBtnSpan = document.querySelector('#selectCityBtn span');
const searchInput = document.getElementById('countryInput');
const countryList = document.getElementById('CountryList');
const searchCityInput = document.getElementById('cityInput');
const cityList = document.getElementById('CityList');
const hiddenInput = document.getElementById('hiddenInput');
const hiddenCityInput = document.getElementById('hiddenCityInput');
let selectedCountry = false;
let selectedCity = false;

function insertCountries(list){
    let li = '';
    list.forEach((name) => {
        if(selectedCountry === name){
            li += `<li onclick="countryClick(this)" class="selected">${name}</li>`;
        }
        else{
            li += `<li onclick="countryClick(this)">${name}</li>`;
        }
    });
    countryList.innerHTML = li;
}
insertCountries(COUNTRY_LIST);
function insertCities(list){
    let li = '';
    list.forEach((name) => {
        if(selectedCountry === name){
            li += `<li onclick="cityClick(this)" class="selected">${name}</li>`;
        }
        else{
            li += `<li onclick="cityClick(this)">${name}</li>`;
        }
    });
    cityList.innerHTML = li;
}
insertCities(COUNTRY_LIST);

selectBtn.onclick = function(){
    selectMenu.classList.toggle('active');
    if(selectMenu.classList.contains('active')) searchInput.focus();
}
selectCityBtn.onclick = function(){
    selectCityMenu.classList.toggle('active');
    if(selectCityMenu.classList.contains('active')) searchCityInput.focus();
}
// When a user click on an option
function countryClick(el){
    selectBtnSpan.innerText = el.innerText;
    selectedCountry = el.innerText;
    hiddenInput.value = el.innerText;
    selectMenu.classList.toggle('active');
    searchInput.value = '';
    insertCountries(COUNTRY_LIST);
}
function cityClick(el){
    selectCityBtnSpan.innerText = el.innerText;
    selectedCity = el.innerText;
    hiddenCityInput.value = el.innerText;
    selectCityMenu.classList.toggle('active');
    searchCityInput.value = '';
    insertCities(COUNTRY_LIST);
}
// When a user search an option
const filteredCountries = function(e){
    let keyword = searchInput.value.toLowerCase();
    let filteredResult = COUNTRY_LIST.filter((country) => {
        country = country.toLowerCase();
        return country.indexOf(keyword) > -1; 
    });
    // console.log(filteredResult);
    insertCountries(filteredResult);
}
const filteredCities = function(e){
    let keyword = searchCityInput.value.toLowerCase();
    let filteredResult = COUNTRY_LIST.filter((country) => {
        country = country.toLowerCase();
        return country.indexOf(keyword) > -1; 
    });
    // console.log(filteredResult);
    insertCities(filteredResult);
}
searchInput.addEventListener('keyup', filteredCountries);
searchCityInput.addEventListener('keyup', filteredCities);